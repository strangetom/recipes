# Recipes

Repository of recipes.

The `assets/` folder contains photos of recipes (original and optimised for web), images of recipe book covers (original and optimised for web).

The `export/` folder contains a json export of the recipe database.
