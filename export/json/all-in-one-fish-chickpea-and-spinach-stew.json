{
  "archiveTimestamp": null,
  "archiveUrl": null,
  "category": "Seafood",
  "cookTime": "PT55M",
  "cuisine": null,
  "dairyFree": false,
  "dateCooked": "2023-07-19",
  "deadUrl": false,
  "description": "The foundations of this dish lie in Marcella Hazan’s famous pasta sauce that calls for only four ingredients; tomatoes, butter, a whole onion and salt. While Hazan instructs to remove the onion, so it merely infuses the tomato sauce, I like a stronger onion flavour and so, in my recipe, the onion is grated, melting into the other ingredients over the course of time. This recipe is simple and clean, allowing the short list of ingredients to do all the talking.",
  "future": false,
  "glutenFree": false,
  "image": "all-in-one-fish-chickpea-and-spinach-stew.webp",
  "inactiveTime": null,
  "ingredient_headings": null,
  "ingredients": [
    {
      "comment": "",
      "fdc_id": "789828",
      "formatted_quantity": "75",
      "group": -1,
      "name": "unsalted butter",
      "prep": "",
      "quantity": "75",
      "synonym": "",
      "unit": "g"
    },
    {
      "comment": "",
      "fdc_id": "333281",
      "formatted_quantity": "2x",
      "group": -1,
      "name": "chopped tomatoes",
      "prep": "",
      "quantity": "2x",
      "synonym": "",
      "unit": "400 g can"
    },
    {
      "comment": "",
      "fdc_id": "2378728",
      "formatted_quantity": "200",
      "group": -1,
      "name": "cherry tomatoes",
      "prep": "halved",
      "quantity": "200",
      "synonym": "",
      "unit": "g"
    },
    {
      "comment": "",
      "fdc_id": "790646",
      "formatted_quantity": "1",
      "group": -1,
      "name": "onion",
      "prep": "peeled and coarsely grated",
      "quantity": "1",
      "synonym": "",
      "unit": ""
    },
    {
      "comment": "",
      "fdc_id": "173470",
      "formatted_quantity": "2",
      "group": -1,
      "name": "of thyme",
      "prep": "",
      "quantity": "2",
      "synonym": "thyme",
      "unit": "sprigs"
    },
    {
      "comment": "",
      "fdc_id": "1104647",
      "formatted_quantity": "1",
      "group": -1,
      "name": "garlic",
      "prep": "finely grated",
      "quantity": "1",
      "synonym": "",
      "unit": "clove"
    },
    {
      "comment": "",
      "fdc_id": "",
      "formatted_quantity": "",
      "group": -1,
      "name": "of chilli flakes",
      "prep": "",
      "quantity": "",
      "synonym": "chilli flakes",
      "unit": "pinch"
    },
    {
      "comment": "",
      "fdc_id": "",
      "formatted_quantity": "8",
      "group": -1,
      "name": "raw, unshelled tiger prawns",
      "prep": "",
      "quantity": "8",
      "synonym": "prawns",
      "unit": ""
    },
    {
      "comment": "",
      "fdc_id": "2644288",
      "formatted_quantity": "1x",
      "group": -1,
      "name": "chickpeas",
      "prep": "drained",
      "quantity": "1x",
      "synonym": "",
      "unit": "400 g can"
    },
    {
      "comment": "",
      "fdc_id": "1999632",
      "formatted_quantity": "125",
      "group": -1,
      "name": "baby spinach",
      "prep": "",
      "quantity": "125",
      "synonym": "spinach",
      "unit": "g"
    },
    {
      "comment": "like cod loin",
      "fdc_id": "",
      "formatted_quantity": "400",
      "group": -1,
      "name": "skinless white fish",
      "prep": "cut into 4 cm (1.5 in) pieces",
      "quantity": "400",
      "synonym": "fish",
      "unit": "g"
    },
    {
      "comment": "to taste",
      "fdc_id": "",
      "formatted_quantity": "",
      "group": -1,
      "name": "juice of 0.5-1 lemon",
      "prep": "",
      "quantity": "",
      "synonym": "",
      "unit": ""
    },
    {
      "comment": "",
      "fdc_id": "",
      "formatted_quantity": "",
      "group": -1,
      "name": "sea salt and freshly ground black pepper",
      "prep": "",
      "quantity": "",
      "synonym": "",
      "unit": ""
    },
    {
      "comment": "to serve",
      "fdc_id": "325871",
      "formatted_quantity": "",
      "group": -1,
      "name": "crusty bread",
      "prep": "",
      "quantity": "",
      "synonym": "",
      "unit": ""
    }
  ],
  "instruction_headings": null,
  "instructions": [
    {
      "group": -1,
      "text": "Add the butter, chopped tomatoes, cherry tomatoes, grated onion, thyme, garlic, chilli flakes, <1> tsp salt and a good grind of black pepper to your deep pot. Remove the heads from the prawns and add them to the pot (these will give great flavour!), reserving the bodies for later."
    },
    {
      "group": -1,
      "text": "Place over a medium heat, bring the pot up to a gentle simmer and then lower the heat, cooking very gently, for {40 minutes@Sauce}, stirring regularly, until the sauce is thick and the fat starts to shimmer on top of the tomatoes."
    },
    {
      "group": -1,
      "text": "Remove the prawn heads with a slotted spoon and transfer to a plate. Discard the thyme stalks. Allow the prawn heads to cool a little and then squeeze them over the pot so all the flavoursome juices are captured. Don’t be squeamish here; this adds delicious rich seafood flavours to the dish and you’ll be missing a trick if you leave this stage out!"
    },
    {
      "group": -1,
      "text": "Add the chickpeas and bring the pot back to a gentle simmer. Add the spinach in a layer on top of the sauce and, using it as a platform, lay the fish and prawns on top. Season. Pop the lid on and cook on a low to medium heat for another {10-12 minutes} or until the fish is cooked through, giving the pot a couple of shakes during this time."
    },
    {
      "group": -1,
      "text": "Give the dish a good grind of black pepper and a generous squeeze of lemon, then serve with some crusty bread on the side."
    }
  ],
  "lowEffort": false,
  "miseEnPlace": false,
  "name": "All-in-one fish, chickpea & spinach stew",
  "notes": null,
  "nutFree": false,
  "nutrition": null,
  "prepTime": "PT25M",
  "rating": "Neutral",
  "slug": "all-in-one-fish-chickpea-and-spinach-stew",
  "source": "Foolproof One-Pot: 60 Simple and Satisfying Recipes",
  "tags": null,
  "totalTime": "PT80M",
  "units": "metric",
  "url": null,
  "vegan": false,
  "yield_": "4"
}